#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define forward 7
#define up 2
#define down 4
int main()
{
    FILE *f;
    long horizontal1 = 0, depth1 = 0, horizontal2 = 0, depth2 = 0, aim = 0;

    if((f = fopen("input.txt", "r")))
    {
        for(int i = 0; i < 1000; i++)
        {
            int length = 0, direction_length = 0, magnitude;
            char c;
            char line[64];

            while((c = fgetc(f)) != '\n' && c != EOF) line[length++] = c;
            line[length] = '\0';
            while(line[direction_length++] != ' ');

            magnitude = atoi(line + direction_length);

            switch(direction_length - 1)
            {
                case forward:
                {
                    horizontal1 += magnitude;  
                    horizontal2 += magnitude;
                    depth2 += magnitude * aim;
                } break;

                case up:
                {
                    depth1 -= magnitude;  
                    aim -= magnitude;
                } break;

                case down:
                {
                    depth1 += magnitude;  
                    aim += magnitude;
                } break;
            }
        }
        
        printf("Part 1: Horizontal1 = %d, depth1 = %d, result = %d\n", horizontal1, depth1, horizontal1 * depth1);
        printf("Part 2: Horizontal2 = %d, depth2 = %d, result = %d\n", horizontal2, depth2, horizontal2 * depth2);
    }

    return 0;
}
