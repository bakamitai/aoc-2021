#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

int main()
{
    FILE *f;
    int numbers[2000];
    int cursor = 0, increase = 0;
    char buffer[64];
    
    if((f = fopen("input.txt", "r")))
    {
        while(fgets(buffer, 64, f))
        {
            int length = strlen(buffer);
            buffer[length - 1] = '\0';
            numbers[cursor++] = atoi(buffer);
        }
        fclose(f);

        for(int i = 0; i < 1999; i++)
        {
            increase += numbers[i + 1] > numbers[i];
        }

        printf("Part one solution = %d\n", increase);

        increase = 0;
        for(int i = 0; i < 1997; i++)
        {
            increase += numbers[i + 3] > numbers[i];
        }

        printf("Part two solution = %d\n", increase);
    }
    else
    {
        fprintf(stderr, "Couldn't open file: %s\n", strerror(errno));
    }
    
    return 0;
}
