#include <stdio.h>
#include "../lib/string.h"

typedef struct
{
    unsigned char data[50];
} Board;

int main()
{
    String file, line;
    Board boards[100] = {0};
    int numbers[100] = {0};
    int offset = 0;

    if(read_to_string("input.txt", &file))
    {
        for(int i = 0; i < 100; i++)
        {
            String n;
            n.data = file.data + offset;
            n.length = 0;

            while(file.data[offset++] != ',' && file.data[offset - 1] != '\n') n.length++;

            numbers[i] = parse_int(n);
        }

        StringIterator iter;
        iter.base = file;
        iter.cursor = 0;
        iter.delimiter = '\n';
        
        next(&iter, &line);
        
        for(int i = 0; i < 100; i++)
        {
            for(int j = 0; j < 5; j++)
            {
                do
                {
                    next(&iter, &line);
                } while(line.length < 1);

                String num;
                StringIterator board_line;
                board_line.base = line;
                board_line.cursor = 0;
                board_line.delimiter = ' ';

                for(int k = 0; k < 5; k++)
                {
                    do
                    {
                        next(&board_line, &num);
                    } while(num.length < 1);

                    int index = (5 * j + k) * 2;
                    boards[i].data[index] = parse_int(num);
                }
            }
        }

        int board_index = 0;
        int called_number = 0;
        for(int i = 0; i < 100; i++)
        {
            int num = numbers[i];
            for(int j = 0; j < 100; j++)
            {
                for(int k = 0; k < 25; k++)
                {
                    if(boards[j].data[2 * k] == num)
                    {
                        boards[j].data[2 * k + 1] = 1;
                        int col = k % 5, row = k / 5, winner = 1;

                        // Row
                        for(int row_index = row * 5; row_index < row * 5 + 5; row_index++)
                        {
                            winner = winner && boards[j].data[row_index * 2 + 1];
                        }
                        if(winner)
                        {
                            board_index = j;
                            called_number = num;
                            goto done;
                        }

                        // Column
                        winner = 1;
                        for(int col_index = col; col_index <= col + 20; col_index += 5)
                        {
                            winner = winner && boards[j].data[col_index * 2 + 1];
                        }
                        if(winner)
                        {
                            board_index = j;
                            called_number = num;
                            goto done;
                        }
                    }
                }
            }
        }
    done:

        int sum = 0;
        for(int i = 0; i < 25; i++)
        {
            if(!boards[board_index].data[i * 2 + 1])
            {
                sum += boards[board_index].data[i * 2];
            }
        }
        printf("Part 1: sum = %d, called number = %d, result = %d\n", sum, called_number, sum * called_number);

        // Part 2

        // Reset selected numbers
        for(int i = 0; i < 100; i++)
        {
            Board *b = boards + i;
            for(int j = 0; j < 25; j++)
            {
                b->data[j * 2 + 1] = 0;
            }
        }

        int end = 100;
        for(int i = 0; i < 100; i++)
        {
            int num = numbers[i];
            for(int j = 0; j < end; j++)
            {
                for(int k = 0; k < 25; k++)
                {
                    if(boards[j].data[2 * k] == num)
                    {
                        boards[j].data[2 * k + 1] = 1;
                        int col = k % 5, row = k / 5, winner = 1;

                        // Row
                        for(int row_index = row * 5; row_index < row * 5 + 5; row_index++)
                        {
                            winner = winner && boards[j].data[row_index * 2 + 1];
                        }
                        if(winner)
                        {
                            called_number = num;
                            Board temp = boards[j];
                            boards[j] = boards[end - 1];
                            boards[end - 1] = temp;
                            j--;
                            end--;
                            break;
                        }

                        // Column
                        winner = 1;
                        for(int col_index = col; col_index <= col + 20; col_index += 5)
                        {
                            winner = winner && boards[j].data[col_index * 2 + 1];
                        }
                        if(winner)
                        {
                            called_number = num;
                            Board temp = boards[j];
                            boards[j] = boards[end - 1];
                            boards[end - 1] = temp;
                            j--;
                            end--;
                            break;
                        }
                    }
                }
            }
        }

        sum = 0;
        for(int i = 0; i < 25; i++)
        {
            if(!boards[end].data[i * 2 + 1])
            {
                sum += boards[end].data[i * 2];
            }
        }

        printf("Part 2: sum = %d, called number = %d, result = %d\n", sum, called_number, sum * called_number);
        
        /* for(int i = 0; i < 100; i++) */
        /* { */
        /*     printf("%d, ", numbers[i]); */
        /* } */
        /* putchar('\n'); */
        /* for(int l = 99; l >= 98; l--) */
        /* { */
        /*     for(int i = 0; i < 5; i++) */
        /*     { */
        /*         for(int j = 0; j < 5; j++) */
        /*         { */
        /*             printf("%d ", boards[l].data[(i * 5 + j) * 2]); */
        /*         } */
        /*         putchar('\n'); */
        /*     } */
        /*     putchar('\n'); */
        /* } */
    }
    return 0;
}
