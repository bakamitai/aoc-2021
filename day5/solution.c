#include <stdio.h>
#include "../lib/string.h"

void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

int main()
{
    StringIterator iter;
    String line;
    int map[1000][1000] = {0};
    int map2[1000][1000] = {0};
    int intersect_count = 0, intersect_count2 = 0;

    if(read_to_string("input.txt", &iter.base))
    {
        int x1, y1, x2, y2;

        iter.cursor = 0;
        iter.delimiter = '\n';

        while(next(&iter, &line))
        {
            sscanf(line.data, "%d,%d -> %d,%d", &x1, &y1, &x2, &y2);

            if(x1 == x2)
            {
                if(y1 > y2) swap(&y1, &y2);
                
                for(int i = y1; i <= y2; i++)
                {
                    intersect_count += ++map[i][x1] == 2;
                    intersect_count2 += ++map2[i][x1] == 2;
                }
            }
            else if(y1 == y2)
            {
                if(x1 > x2) swap(&x1, &x2);
                
                for(int i = x1; i <= x2; i++)
                {
                    intersect_count += ++map[y1][i] == 2;
                    intersect_count2 += ++map2[y1][i] == 2;
                }
            }
            else
            {
                int xstep = x1 > x2 ? -1 : 1;
                int ystep = y1 > y2 ? -1 : 1;
                int length = (x1 - x2) > 0 ? x1 - x2 : x2 - x1;

                for(int i = 0; i <= length; i++)
                {
                    intersect_count2 += ++map2[y1][x1] == 2;
                    y1 += ystep;
                    x1 += xstep;
                }
            }
        }

        printf("Part 1: intersect count = %d\n", intersect_count);
        printf("Part 2: intersect count = %d\n", intersect_count2);
    }
    return 0;
}
