#include "string.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>

int next(StringIterator *iter, String *output)
{
    if(iter->cursor >= iter->base.length) return 0;

    output->data = iter->base.data + iter->cursor;
    output->length = 0;

    while(iter->cursor < iter->base.length && iter->base.data[iter->cursor++] != iter->delimiter) output->length++;
    
    output->capacity = output->length;
    return 1;
}

int read_to_string(char *filename, String *output)
{
    struct stat statbuf = {0};

    if(stat(filename, &statbuf) == 0)
    {
        FILE *f;
        if((f = fopen(filename, "r")))
        {
            output->data = (char *) malloc(statbuf.st_size);
            output->length = output->capacity = statbuf.st_size;
            fread(output->data, 1, statbuf.st_size, f);
            fclose(f);
        }
        else
        {
            fprintf(stderr, "Unable to open file %s\n\t%s\n", filename, strerror(errno));
            return 0;
        }
    }
    else
    {
        fprintf(stderr, "Unable to open file %s\n\t%s\n", filename, strerror(errno));
        return 0;
    }

    return 1;
}

String copy_string(String s)
{
    String new;
    new.data = (char *) malloc(s.length);
    new.length = s.length;
    new.capacity = s.length;
    memcpy(new.data, s.data, s.length);
    return new;
}

int parse_int(String s)
{
    int order = 1;
    int result = 0;
    for(int i = s.length - 1; i >= 0; i--)
    {
        result += (s.data[i] - '0') * order;
        order *= 10;
    }
    return result;
}

String strip(String s)
{
    char c;
    String stripped = s;
    
    while((c = stripped.data[stripped.length - 1]) == ' ' || c == '\t' || c == '\n' || c == '\r') stripped.length--;
    while((c = stripped.data[0]) == ' ' || c == '\t' || c == '\n' || c == '\r')
    {
        stripped.data++;
        stripped.length--;
    }

    return stripped;
}

int find(String s, char c)
{
    for(int i = 0; i < s.length; i++)
    {
        if(s.data[i] == c) return i;
    }

    return -1;
}

String split_left(String s, int index)
{
    String left;
    left.data = s.data;
    left.length = index + 1;
    left.capacity = left.length;

    return left;
}

String split_right(String s, int index)
{
    String right;
    right.data = s.data + index;
    right.length = s.length - index;
    right.capacity = right.length;

    return right;
}

String substring(String s, int start, int end)
{
    String sub;
    sub.data = s.data + start;
    sub.length = end - start;
    sub.capacity = sub.length;
    return sub;
}

int common_chars(String a, String b)
{
    int common = 0;
    for(int i = 0; i < a.length; i++)
    {
        for(int j = 0; j < b.length; j++)
        {
            if(a.data[i] == b.data[j])
            {
                common++;
                break;
            }
        }
    }
    return common;
}

int equal(String a, String b)
{
    if(a.length != b.length) return 0;

    for(int i = 0; i < a.length; i++)
    {
        char c = a.data[i];
        int matched = 0;
        for(int j = 0; j < b.length; j++)
        {
            if(b.data[j] == c)
            {
                matched = 1;
                break;
            }
        }
        if(!matched) return 0;
    }
    return 1;
}

void print(String s)
{
    for(int i = 0; i < s.length; i++)
    {
        putchar(s.data[i]);
    }
    putchar('\n');
}
