#ifndef STRING
#define STRING

typedef struct
{
    char *data;
    int length;
    int capacity;
} String;

typedef struct
{
    String base;
    char delimiter;
    int cursor;
} StringIterator;

int next(StringIterator *iter, String *output);
int read_to_string(char *filename, String *output);
String copy_string(String s);
int parse_int(String s);
String strip(String s);
int find(String s, char c);
String split_left(String s, int index);
String split_right(String s, int index);
String substring(String s, int start, int end);
int common_chars(String a, String b);
int equal(String a, String b);
void print(String s);

#endif
