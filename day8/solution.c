#include <stdio.h>
#include <assert.h>
#include "../lib/string.h"

#if 0
#define EXAMPLE
#define FILENAME "example.txt"
#else
#define FILENAME "input.txt"
#endif

#define NUM_SEGMENTS_0 6
#define NUM_SEGMENTS_1 2
#define NUM_SEGMENTS_2 5
#define NUM_SEGMENTS_3 5
#define NUM_SEGMENTS_4 4
#define NUM_SEGMENTS_5 5
#define NUM_SEGMENTS_6 6
#define NUM_SEGMENTS_7 3
#define NUM_SEGMENTS_8 7
#define NUM_SEGMENTS_9 6
#define UNIQUE_LEN(n) ((n) == NUM_SEGMENTS_1 || (n) == NUM_SEGMENTS_4 || (n) == NUM_SEGMENTS_7 || (n) == NUM_SEGMENTS_8)

#define SEG_MASK_0 (1 | (1 << 1) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5))
#define SEG_MASK_1 ((1 << 1) | (1 << 2))
#define SEG_MASK_2 (1 | (1 << 1) | (1 << 6) | (1 << 4) | (1 << 3))
#define SEG_MASK_3 (1 | (1 << 1) | (1 << 2) | (1 << 3) | (1 << 6))
#define SEG_MASK_4 ((1 << 1) | (1 << 2) | (1 << 5) | (1 << 6))
#define SEG_MASK_5 (1 | (1 << 5) | (1 << 6) | (1 << 2) | (1 << 3))
#define SEG_MASK_6 (1 | (1 << 5) | (1 << 4) | (1 << 3) | (1 << 2) | (1 << 6))
#define SEG_MASK_7 (1 | (1 << 1) | (1 << 2))
#define SEG_MASK_8 127
#define SEG_MASK_9 (1 | (1 << 1) | (1 << 2) | (1 << 3) | (1 << 5) | (1 << 6))

/*
       0
   # # # # # 
   #       #
 5 #       # 1
   #   6   #
   # # # # #
   #       #
 4 #       # 2
   #       #
   # # # # #
       3
 */

int main()
{
    StringIterator lines, values;
    String line, value;

    if(read_to_string(FILENAME, &lines.base))
    {
        lines.cursor = 0;
        lines.delimiter = '\n';

        int one_four_seven_or_eight = 0;
        while(next(&lines, &line))
        {
            values.base = strip(split_right(line, find(line, '|') + 1));
            values.cursor = 0;
            values.delimiter = ' ';

            while(next(&values, &value))
            {
                one_four_seven_or_eight += UNIQUE_LEN(value.length);
            }
        }

        #ifdef EXAMPLE
        assert(one_four_seven_or_eight == 26);
        #else
        printf("Part 1: %d\n", one_four_seven_or_eight);
        #endif

        /* unsigned char segmasks[10] = { */
        /*     SEG_MASK_0, */
        /*     SEG_MASK_1, */
        /*     SEG_MASK_2, */
        /*     SEG_MASK_3, */
        /*     SEG_MASK_4, */
        /*     SEG_MASK_5, */
        /*     SEG_MASK_6, */
        /*     SEG_MASK_7, */
        /*     SEG_MASK_8, */
        /*     SEG_MASK_9, */
        /* }; */

        /* for(int i = 0; i < 9; i++) */
        /* { */
        /*     for(int j = i + 1; j < 10; j++) */
        /*     { */
        /*         int common = 0; */
        /*         for(int k = 0; k < 7; k++) */
        /*         { */
        /*             common += ((segmasks[i] >> k) & 1) && ((segmasks[j] >> k) & 1); */
        /*         } */
        /*         printf("%d ", common); */
        /*     } */
        /*     putchar('\n'); */
        /* } */

        // Part 2
        lines.cursor = 0;
        unsigned total = 0;
        while(next(&lines, &line))
        {
            StringIterator left, right;
            int split_point = find(line, '|');

            left.base = strip(split_left(line, split_point - 1));
            left.cursor = 0;
            left.delimiter = ' ';
            
            right.base = strip(split_right(line, split_point + 1));
            right.cursor = 0;
            right.delimiter = ' ';

            // Length 2 strings: 1
            // Length 3 strings: 7
            // Length 4 strings: 4
            // Length 5 strings: 2, 3, 5
            // Length 6 strings: 0, 6, 9
            // Length 7 strings: 8
            String one, seven, four, eight, input;
            String five_length_strings[3];
            String six_length_strings[3];
            String string_number_map[10];
            int five_length = 0, six_length = 0;

            while(next(&left, &input))
            {
                switch(input.length)
                {
                    case 2: one = input; break;
                    case 3: seven = input; break;
                    case 4: four = input; break;
                    case 5: five_length_strings[five_length++] = input; break;
                    case 6: six_length_strings[six_length++] = input; break;
                    case 7: eight = input; break;
                }
            }

            string_number_map[1] = one;
            string_number_map[4] = four;
            string_number_map[7] = seven;
            string_number_map[8] = eight;

            // 9

            if(common_chars(six_length_strings[0], four) == 4)
            {
                string_number_map[9] = six_length_strings[0];
                six_length_strings[0] = six_length_strings[2];
            }
            else if(common_chars(six_length_strings[1], four) == 4)
            {
                string_number_map[9] = six_length_strings[1];
                six_length_strings[1] = six_length_strings[2];
            }
            else
            {
                string_number_map[9] = six_length_strings[2];
            }

            // 6 0

            if(common_chars(six_length_strings[0], one) == 2)
            {
                string_number_map[0] = six_length_strings[0];
                string_number_map[6] = six_length_strings[1];
            }
            else
            {
                string_number_map[0] = six_length_strings[1];
                string_number_map[6] = six_length_strings[0];
            }

            // 2

            if(common_chars(five_length_strings[0], four) == 2)
            {
                string_number_map[2] = five_length_strings[0];
                five_length_strings[0] = five_length_strings[2];
            }
            else if(common_chars(five_length_strings[1], four) == 2)
            {
                string_number_map[2] = five_length_strings[1];
                five_length_strings[1] = five_length_strings[2];
            }
            else
            {
                string_number_map[2] = five_length_strings[2];
            }

            // 3 5
            
            if(common_chars(five_length_strings[0], seven) == 3)
            {
                string_number_map[3] = five_length_strings[0];
                string_number_map[5] = five_length_strings[1];
            }
            else
            {
                string_number_map[3] = five_length_strings[1];
                string_number_map[5] = five_length_strings[0];
            }

            // Should be everything LOL!!!!!
            String output;
            unsigned order = 1000;
            unsigned value = 0;
            while(next(&right, &output))
            {
                for(int i = 0; i < 10; i++)
                {
                    if(equal(output, string_number_map[i]))
                    {
                        value += i * order;
                        order /= 10;
                        break;
                    }
                }
            }
            total += value;
        }

        printf("Part 2: %d\n", total);
    }
    
    return 0;
}
