#include <stdio.h>
#include <stdlib.h>
#include "../lib/string.h"

#define POSITION_SIZE 1000
#define abs(n) ((n) >= 0 ? (n) : -(n))

int main()
{
    int positions[POSITION_SIZE];
    StringIterator iter;
    String num;

    if(read_to_string("input.txt", &iter.base))
    {
        iter.cursor = 0;
        iter.delimiter = ',';

        for(int i = 0; i < POSITION_SIZE; i++)
        {
            next(&iter, &num);
            num = strip(num);
            positions[i] = parse_int(num);
        }
        free(iter.base.data);

        // Part 1
        int min_fuel1 = 0x7fffffff;
        int min_fuel2 = 0x7fffffff;
        int min_pos1, min_pos2;

        for(int pos = 0; pos < 2000; pos++)
        {
            int fuel1 = 0, fuel2 = 0;
            for(int j = 0; j < POSITION_SIZE; j++)
            {
                int dist = abs(pos - positions[j]);
                fuel1 += dist;
                fuel2 += (dist * (dist + 1)) / 2;
            }
            if(fuel1 < min_fuel1)
            {
                min_fuel1 = fuel1;
                min_pos1 = pos;
            }
            if(fuel2 < min_fuel2)
            {
                min_fuel2 = fuel2;
                min_pos2 = pos;
            }
        }
        printf("Part 1: Horizontal position = %d, fuel usage = %d\n", min_pos1, min_fuel1);
        printf("Part 2: Horizontal position = %d, fuel usage = %d\n", min_pos2, min_fuel2);
    }
}
