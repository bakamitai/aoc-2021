#include <stdio.h>
#include <stdint.h>
#include "../lib/string.h"

uint64_t baby_tree(int age, int remaining_days)
{
    uint64_t return_val = 1;
    remaining_days -= age + 1;
    while(remaining_days >= 0)
    {
        return_val += baby_tree(8, remaining_days);
        remaining_days -= 7;
    }
    return return_val;
}

int main()
{
    String file;
    uint64_t ages[9] = {0};

    if(read_to_string("input.txt", &file))
    {
        for(int i = 0; i < file.length; i++)
        {
            if(file.data[i] >= '0' && file.data[i] <= '9') ages[file.data[i] - '0']++;
        }

        // Part 1

        uint64_t total = 0;

        for(int i = 0; i < 9; i++)
        {
            if(ages[i] > 0) total += baby_tree(i, 80) * ages[i];
        }
        
        printf("Part 1: %lu\n", total);

        // Part 2
        
        total = 0;
        for(int i = 0; i < 256; i++)
        {
            uint64_t zeroes = ages[0];
            for(int j = 1; j < 9; j++)
            {
                ages[j - 1] = ages[j];
            }
            ages[8] = zeroes;
            ages[6] += zeroes;
        }

        for(int i = 0; i < 9; i++)
        {
            total += ages[i];
        }
        printf("Part 2: %lu\n", total);
    }

    return 0;
}
