#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include "../lib/string.h"

#if 0

#define EXAMPLE
#define FILENAME "example.txt"

#else

#define FILENAME "input.txt"

#endif

#define GRID_LENGTH 10

static char octopussies[GRID_LENGTH][GRID_LENGTH];
static char flashed_octopussies[GRID_LENGTH][GRID_LENGTH];

int main()
{
    StringIterator lines;
    String line;

    if(read_to_string(FILENAME, &lines.base))
    {
        lines.cursor = 0;
        lines.delimiter = '\n';
        int yy = 0;

        while(next(&lines, &line))
        {
            for(int x = 0; x < line.length; x++)
            {
                octopussies[yy][x] = line.data[x] - '0';
            }
            yy++;
        }

        uint64_t flashes = 0;
        int step = 0;
        for(;;)
        {
            if(step == 100)
            {
#ifdef EXAMPLE
                assert(flashes == 1656);
#else
                printf("%lu\n", flashes);
#endif
            }

            int done = 1;
            for(int y = 0; y < GRID_LENGTH; y++)
            {
                for(int x = 0; x < GRID_LENGTH; x++)
                {
                    if(octopussies[y][x]) done = 0;
                }
            }
            if(done)
            {
                #ifdef EXAMPLE
                assert(step == 195);
                #else
                printf("%d\n", step);
                #endif
                break;
            }
            int flashed = 0;

            // Increment all the cunts
            for(int y = 0; y < GRID_LENGTH; y++)
            {
                for(int x = 0; x < GRID_LENGTH; x++)
                {
                    if(++octopussies[y][x] > 9)
                    {
                        flashed = 1;
                        flashed_octopussies[y][x] = 1;
                        flashes++;
                    }
                }
            }

            // Apply flashes to the other cunts
            while(flashed)
            {
                flashed = 0;
                for(int y = 0; y < GRID_LENGTH; y++)
                {
                    for(int x = 0; x < GRID_LENGTH; x++)
                    {
                        if(flashed_octopussies[y][x] == 1)
                        {
                            flashed_octopussies[y][x] += 1;
                            // Left

                            if(x > 0)
                            {
                                if(!flashed_octopussies[y][x - 1] && ++octopussies[y][x - 1] > 9)
                                {
                                    flashed = 1;
                                    flashes++;
                                    flashed_octopussies[y][x - 1] = 1;
                                }
                            }

                            // Right
                            if(x < GRID_LENGTH - 1)
                            {
                                if(!flashed_octopussies[y][x + 1] && ++octopussies[y][x + 1] > 9)
                                {
                                    flashed = 1;
                                    flashes++;
                                    flashed_octopussies[y][x + 1] = 1;
                                }
                            }

                            // Up
                            if(y > 0)
                            {
                                if(!flashed_octopussies[y - 1][x] && ++octopussies[y - 1][x] > 9)
                                {
                                    flashed = 1;
                                    flashes++;
                                    flashed_octopussies[y - 1][x] = 1;
                                }

                                // Up left
                                if(x > 0)
                                {
                                    if(!flashed_octopussies[y - 1][x - 1] && ++octopussies[y - 1][x - 1] > 9)
                                    {
                                        flashed = 1;
                                        flashes++;
                                        flashed_octopussies[y - 1][x - 1] = 1;
                                    }
                                }

                                // Up right
                                if(x < GRID_LENGTH - 1)
                                {
                                    if(!flashed_octopussies[y - 1][x + 1] && ++octopussies[y - 1][x + 1] > 9)
                                    {
                                        flashed = 1;
                                        flashes++;
                                        flashed_octopussies[y - 1][x + 1] = 1;
                                    }
                                }
                            }

                            // Down
                            if(y < GRID_LENGTH - 1)
                            {
                                if(!flashed_octopussies[y + 1][x] && ++octopussies[y + 1][x] > 9)
                                {
                                    flashed = 1;
                                    flashes++;
                                    flashed_octopussies[y + 1][x] = 1;
                                }

                                // Down left
                                if(x > 0)
                                {
                                    if(!flashed_octopussies[y + 1][x - 1] && ++octopussies[y + 1][x - 1] > 9)
                                    {
                                        flashed = 1;
                                        flashes++;
                                        flashed_octopussies[y + 1][x - 1] = 1;
                                    }
                                }

                                // Down right
                                if(x < GRID_LENGTH - 1)
                                {
                                    if(!flashed_octopussies[y + 1][x + 1] && ++octopussies[y + 1][x + 1] > 9)
                                    {
                                        flashed = 1;
                                        flashes++;
                                        flashed_octopussies[y + 1][x + 1] = 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /* printf("Step %d Before zero\n", i + 1); */
            /* for(int y = 0; y < GRID_LENGTH; y++) */
            /* { */
            /*     for(int x = 0; x < GRID_LENGTH; x++) */
            /*     { */
            /*         printf("%d", octopussies[y][x]); */
            /*     } */
            /*     putchar('\n'); */
            /* } */
            /* putchar('\n'); */

            // Set all flashed cunts to 0
            for(int y = 0; y < GRID_LENGTH; y++)
            {
                for(int x = 0; x < GRID_LENGTH; x++)
                {
                    if(flashed_octopussies[y][x])
                    {
                        octopussies[y][x] = 0;
                        flashed_octopussies[y][x] = 0;
                    }
                }
            }

            step++;
            /* printf("Step %d\n", i + 1); */
            /* for(int y = 0; y < GRID_LENGTH; y++) */
            /* { */
            /*     for(int x = 0; x < GRID_LENGTH; x++) */
            /*     { */
            /*         printf("%d", octopussies[y][x]); */
            /*     } */
            /*     putchar('\n'); */
            /* } */
            /* putchar('\n'); */
        }


        /* for(y = 0; y < GRID_LENGTH; y++) */
        /* { */
        /*     for(int x = 0; x < GRID_LENGTH; x++) */
        /*     { */
        /*         putchar(octopussies[y][x] + '0'); */
        /*     } */
        /*     putchar('\n'); */
        /* } */
    }

    return 0;
}
