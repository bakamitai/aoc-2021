(defun sexy-compile ()
  (interactive)
  (let ((bn (buffer-name)))
    (cond
     ((or (string= "string.h" bn) (string= "string.c" bn))
      (compile "cc -Wall -O2 -c string.c"))

     ((string= "solution.c" bn)
      (compile "cc -Wall -c solution.c && cc -o sol solution.o ../lib/string.o"))

     ((message "Unrecognized file name")))))

(global-set-key (kbd "<f6>") 'sexy-compile)
