#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include "../lib/string.h"

#if 0

#define EXAMPLE
#define FILENAME "example.txt"

#else

#define FILENAME "input.txt"

#endif

#define VALID 1
#define INCOMPLETE 2
#define CORRUPTED 3

static uint64_t autocomplete_score;

int parse_line(char **line_ptr, char close, int depth)
{
    while(**line_ptr)
    {
        switch(**line_ptr)
        {
            case '(':
            {
                line_ptr[0]++;
                int result = parse_line(line_ptr, ')', depth + 1);
                if(result) return result;
            } break;

            case '[':
            {
                line_ptr[0]++;
                int result = parse_line(line_ptr, ']', depth + 1);
                if(result) return result;
            } break;

            case '{':
            {
                line_ptr[0]++;
                int result = parse_line(line_ptr, '}', depth + 1);
                if(result) return result;
            } break;

            case '<':
            {
                line_ptr[0]++;
                int result = parse_line(line_ptr, '>', depth + 1);
                if(result) return result;
            } break;

            default:
            {
                if(**line_ptr == close)
                {
                    line_ptr[0]++;
                    if(depth) return 0;
                    if(**line_ptr == '(') close = **line_ptr + 1;
                    else close = **line_ptr + 2;
                    line_ptr[0]++;
                }
                switch(**line_ptr)
                {
                    case ')': return 3;
                    case ']': return 57;
                    case '}': return 1197;
                    case '>': return 25137;
                }
            } break;
        }
    }
    autocomplete_score *= 5;
    switch(close)
    {
        case ')': autocomplete_score += 1; break;
        case ']': autocomplete_score += 2; break;
        case '}': autocomplete_score += 3; break;
        case '>': autocomplete_score += 4; break;
    }
    return 0;
}

void sort(uint64_t *arr, int length)
{
    for(int i = 0; i < length; i++)
    {
        for(int j = 0; j < length - i - 1; j++)
        {
            if(arr[j] > arr[j + 1])
            {
                uint64_t temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

int main()
{
    StringIterator lines;
    String line;
    uint64_t autocomplete_scores[256] = {0};
    int autocomplete_length = 0;

    if(read_to_string(FILENAME, &lines.base))
    {
        lines.cursor = 0;
        lines.delimiter = '\n';
        int syntax_score = 0;
        while(next(&lines, &line))
        {
            line.data[line.length] = '\0';

            char close;
            if(*line.data == '(') close = *line.data + 1;
            else close = *line.data + 2;

            line.data++;

            autocomplete_score = 0;
            syntax_score += parse_line(&line.data, close, 0);
            if(autocomplete_score) autocomplete_scores[autocomplete_length++] = autocomplete_score;
        }
        assert(autocomplete_length & 1);
        sort(autocomplete_scores, autocomplete_length);

        #ifdef EXAMPLE
        assert(syntax_score == 26397);
        assert(autocomplete_scores[autocomplete_length / 2] == 288957);
        #else
        printf("Part 1: %d\n", syntax_score);
        printf("Part 2: %lu\n", autocomplete_scores[autocomplete_length / 2]);
        #endif
    }

    return 0;
}
