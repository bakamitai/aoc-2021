#include <stdio.h>
#include "../lib/string.h"

int main()
{
    int number[12] = {0};
    int epsilon = 0, gamma = 0, candidates = 1000;
    String line;
    StringIterator iter;
    String numbers[1000];
    int index = 0;

    if(read_to_string("input.txt", &iter.base))
    {
        // Part 1
        iter.cursor = 0;
        iter.delimiter = '\n';

        while(next(&iter, &line))
        {
            numbers[index++] = line;
            for(int i = 0; i < line.length; i++)
            {
                if(line.data[i] == '1') number[11 - i]++;
                else number[11 - i]--;
            }
        }

        for(int i = 0; i < 12; i++)
        {
            gamma |= (number[i] > 0) << i;
        }
        epsilon = ~gamma & 0xfff;

        printf("Part 1: gamma = %d, epsilon = %d, result = %d\n", gamma, epsilon, gamma * epsilon);

        // Part 2
        int bit_index = 0;
        while(candidates > 1)
        {
            int most_common = 0, end = 0;
            char filter_value;

            for(int i = 0; i < candidates; i++)
            {
                if(numbers[i].data[bit_index] == '1') most_common++;
                else most_common--;
            }

            filter_value = most_common >= 0 ? '1' : '0';

            for(int i = 0; i < candidates; i++)
            {
                if(numbers[i].data[bit_index] == filter_value)
                {
                    String temp = numbers[i];
                    numbers[i] = numbers[end];
                    numbers[end] = temp;
                    end++;
                }
            }
            candidates = end;
            bit_index++;
        }

        int oxygen = 0;
        for(int i = 0; i < numbers[0].length; i++)
        {
            oxygen |= (numbers[0].data[i] == '1') << (11 - i);
        }

        bit_index = 0;
        candidates = 1000;
        
        while(candidates > 1)
        {
            int most_common = 0, end = 0;
            char filter_value;

            for(int i = 0; i < candidates; i++)
            {
                if(numbers[i].data[bit_index] == '1') most_common++;
                else most_common--;
            }

            filter_value = most_common >= 0 ? '0' : '1';

            for(int i = 0; i < candidates; i++)
            {
                if(numbers[i].data[bit_index] == filter_value)
                {
                    String temp = numbers[i];
                    numbers[i] = numbers[end];
                    numbers[end] = temp;
                    end++;
                }
            }
            candidates = end;
            bit_index++;
        }

        int carbon = 0;
        for(int i = 0; i < numbers[0].length; i++)
        {
            carbon |= (numbers[0].data[i] == '1') << (11 - i);
        }
        printf("Part 2: oxygen = %d, carbon = %d, result = %d\n", oxygen, carbon, oxygen * carbon);
    }

    return 0;
}
