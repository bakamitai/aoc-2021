#include <stdio.h>
#include <assert.h>
#include "../lib/string.h"

#if 0

#define EXAMPLE
#define FILENAME "example.txt"
#define MAP_WIDTH  10
#define MAP_HEIGHT 5

#else

#define MAP_WIDTH  100
#define MAP_HEIGHT 100
#define FILENAME "input.txt"

#endif

static char map[MAP_HEIGHT][MAP_WIDTH];
static char basin_map[MAP_HEIGHT][MAP_WIDTH];

void clear()
{
    for(int y = 0; y < MAP_HEIGHT; ++y)
    {
        for(int x = 0; x < MAP_WIDTH; ++x)
        {
            basin_map[y][x] = 0;
        }
    }
}

void insert_basin_size(int *basins, int basin_size)
{
    if(basin_size < basins[0]) return;
    int temp;
    
    basins[0] = basin_size;

    if(basins[0] > basins[1])
    {
        temp = basins[0];
        basins[0] = basins[1];
        basins[1] = temp;
    }
    
    if(basins[1] > basins[2])
    {
        temp = basins[1];
        basins[1] = basins[2];
        basins[2] = temp;
    }
}

int measure_basin(int x, int y)
{
    if(map[y][x] == 9 || basin_map[y][x]) return 0;
    basin_map[y][x] = 1;
    
    int result = 1;
    
    // Left

    if(x > 0) result += measure_basin(x - 1, y);

    // Right

    if(x < MAP_WIDTH - 1) result += measure_basin(x + 1, y);

    // Up

    if(y > 0) result += measure_basin(x, y - 1);

    // Down
                
    if(y < MAP_HEIGHT - 1) result += measure_basin(x, y + 1);

    return result;
}

int main()
{
    StringIterator lines;
    String line;

    if(read_to_string(FILENAME, &lines.base))
    {
        lines.cursor = 0;
        lines.delimiter = '\n';

        for(int y = 0; y < MAP_HEIGHT; ++y)
        {
            next(&lines, &line);
            
            for(int x = 0; x < MAP_WIDTH; ++x)
            {
                map[y][x] = line.data[x] - '0'; 
            }
        }

        int risk_sum = 0;
        int basins[3] = {0};

        for(int y = 0; y < MAP_HEIGHT; ++y)
        {
            for(int x = 0; x < MAP_WIDTH; ++x)
            {
                int low_point = 1;

                // Left

                if(x > 0) low_point = low_point && (map[y][x] < map[y][x - 1]);

                // Right

                if(x < MAP_WIDTH - 1) low_point = low_point && (map[y][x] < map[y][x + 1]);

                // Up

                if(y > 0) low_point = low_point && (map[y][x] < map[y - 1][x]);

                // Down
                
                if(y < MAP_HEIGHT - 1) low_point = low_point && (map[y][x] < map[y + 1][x]);

                if(low_point)
                {
                    risk_sum += map[y][x] + 1;
                    insert_basin_size(basins, measure_basin(x, y));
                    clear();
                }
            }
        }

        #ifdef EXAMPLE
        assert(risk_sum == 15);
        assert(basins[0] * basins[1] * basins[2] == 1134);
        #else
        printf("Part 1: %d\n", risk_sum);
        printf("Part 2: %d\n", basins[0] * basins[1] * basins[2]);
        #endif
    }

    return 0;
}
